const express = require("express");
const router = express.Router();
const adminControllers = require("../Controllers/admin");
const isAuth = require("../Middlewares/is-auth");

/* GET */
router.get("/verifyToken", isAuth.verifyToken);
router.get("/getNewOrdersCount", isAuth.isAuth,adminControllers.getNewOrdersCount);

router.get(
	"/getCategories",
	isAuth.isAuth,
	adminControllers.fetchAllCategories
);
router.get(
	"/deleteCategory/:id",
	isAuth.isAuth,
	adminControllers.deleteCategory
);
router.get(
	"/ActivateOrDeactivateCategory/:id/:isActive",
	isAuth.isAuth,
	adminControllers.ActivateOrDeactivateCategory
);

router.get("/getProducts/:id", isAuth.isAuth, adminControllers.getProducts);
router.get("/deleteProduct/:id", isAuth.isAuth, adminControllers.deleteProduct);

router.get("/getOpenOrders", isAuth.isAuth, adminControllers.getOpenOrders);
router.get("/getClosedOrders", isAuth.isAuth, adminControllers.getClosedOrders);
router.get("/getNewOrders", isAuth.isAuth, adminControllers.getNewOrders);
/* POST */
router.post("/login", adminControllers.login);

router.post("/addNewCategory", isAuth.isAuth, adminControllers.addNewCategory);
router.post("/updateCategory", isAuth.isAuth, adminControllers.updateCategory);
router.post(
	"/updateOrderStatus",
	isAuth.isAuth,
	adminControllers.updateOrderStatus
);

router.post("/addNewProduct", isAuth.isAuth, adminControllers.addNewProduct);
router.post("/markAsReadOrders", isAuth.isAuth, adminControllers.markAsReadOrders);
router.post(
	"/updateProduct/:id",
	isAuth.isAuth,
	adminControllers.updateProduct
);

module.exports = router;
