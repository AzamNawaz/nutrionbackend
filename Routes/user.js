const express = require("express");
const router = express.Router();
const userControllers = require("../Controllers/user");
const isAuth = require("../Middlewares/is-auth");

router.get("/getCategories", userControllers.getCategories);
router.post("/checkout", userControllers.checkout);
router.get("/getProducts/:id", userControllers.getProducts);
router.post("/contactInforamtion", userControllers.contactInforamtion);

module.exports = router;
