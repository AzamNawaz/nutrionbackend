const Category = require("../Models/category");
const Product = require("../Models/product");
const Order = require("../Models/order");
const Contact = require("../Models/contact");
const io = require("../socket");
exports.getCategories = (req, res, next) => {
	Category.find({ isActive: true }, "title image")
		.then((categories) => {
			res.json({
				info: {
					status: 200,
				},
				categories: categories,
			});
		})
		.catch((error) => {
			next(error);
		});
};
exports.getProducts = (req, res, next) => {
	const id = req.params.id;
	Product.find(
		{ category: id, isActive: true },
		"title stock price image description units"
	)
		.then((products) => {
			res.json({
				info: {
					status: 200,
				},
				products: products,
			});
		})
		.catch((error) => {
			next(error);
		});
};
exports.checkout = (req, res, next) => {
	const shippingDetails = req.body.shippingDetails;
	const userDetails = req.body.userDetails;
	const products = req.body.products;
	const total = req.body.total;
	if (!shippingDetails.address) {
		const error = new Error("Address is required");
		error.status = 400;
		throw error;
	}
	if (!shippingDetails.city) {
		const error = new Error("City is required");
		error.status = 400;
		throw error;
	}
	if (!userDetails.firstName) {
		const error = new Error("First name is required");
		error.status = 400;
		throw error;
	}
	if (!userDetails.lastName) {
		const error = new Error("Last name is required");
		error.status = 400;
		throw error;
	}
	if (!userDetails.mobile) {
		const error = new Error("Mobile number is required");
		error.status = 400;
		throw error;
	}
	if (products ? !products.length : true) {
		const error = new Error("No products are added in the cart");
		error.status = 400;
		throw error;
	}
	const order = new Order({
		shippingDetails: shippingDetails,
		userDetails: userDetails,
		deliveryCharges: 200,
		products: products,
		total: total,
	});
	order
		.save()
		.then((order) => {
			io.getIO().emit("order", { action: "create", order: order });
			res.json({
				info: {
					status: 200,
					message: "Order placed successfully",
				},
				order: order,
			});
		})
		.catch((error) => {
			next(error);
		});
};
exports.contactInforamtion = (req, res, next) => {
	const firstName = req.body.firstName;
	const lastName = req.body.lastName;
	const email = req.body.email;
	const mobile = req.body.mobile;
	const subject = req.body.subject;
	const description = req.body.description;
	console.log(firstName, lastName, mobile, subject, description);
	if (!firstName || !lastName || !mobile || !subject || !description) {
		return res.json({
			info: {
				status: 400,
				message: "Enter all required fields",
			},
		});
	}
	const contact = new Contact({
		firstName: firstName,
		lastName: lastName,
		email: email,
		mobile: mobile,
		subject: subject,
		description: description,
	});
	contact
		.save()
		.then((result) => {
			if (!result) {
				const error = new Error("Something went wrong please try again later");
				error.status = 400;
				throw error;
			}
			res.json({
				info: {
					status: 200,
					message:
						"Thank you for contacting us we will reach you as soon as possible",
				},
			});
		})
		.catch((error) => {
			next(error);
		});
};
