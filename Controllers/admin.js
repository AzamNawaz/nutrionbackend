const admin = require("../Models/user");
const Category = require("../Models/category");
const Product = require("../Models/product");
const Order = require("../Models/order");
const path = require("path");
const jwt = require("jsonwebtoken");
const fs = require("fs");
const io = require("../socket");
exports.login = (req, res, next) => {
	const email = req.body.email;
	const pass = req.body.password;
	if (!email) {
		const error = new Error("Enter Email Address");
		error.status = 400;
		next(error);
	}
	if (!pass) {
		const error = new Error("Enter Password");
		error.status = 400;
		next(error);
	}
	let loadedUser;
	admin
		.findOne({ email: email })
		.then((user) => {
			if (!user) {
				const error = new Error("User Not Found");
				error.status = 400;
				throw error;
			}
			loadedUser = user;
			let matchPass = pass == user.password;
			if (!matchPass) {
				const error = new Error("Password Incorrect");
				error.status = 400;
				throw error;
			}
			const token = jwt.sign(
				{
					email: loadedUser.email,
					name: loadedUser.name,
					userId: loadedUser._id.toString(),
				},
				"mysecretkey",
				{ expiresIn: "1h" }
			);
			return res.json({
				info: {
					status: 200,
					message: "Logged in Successfully",
				},
				userName: user.name,
				token: token,
			});
		})
		.catch((err) => {
			next(err);
		});
};
exports.addNewCategory = (req, res, next) => {
	const title = req.body.title;
	const userId = req.userId;
	let image;
	if (req.file) {
		image = req.file.path;
	}
	if (!title) {
		const error = new Error("Enter Title of Category");
		error.status = 400;
		next(error);
	}
	if (!image) {
		const error = new Error("Image Not Found");
		error.status = 400;
		next(error);
	}

	Category.findOne({ title: title })
		.then((found) => {
			if (found) {
				const error = new Error("Category already exists");
				error.status = 400;
				throw error;
			}
			const category = new Category({
				title: title,
				createdBy: userId,
				isActive: true,
				image: image,
			});
			return category.save();
		})
		.then((cat) => {
			if (!cat) {
				const error = new Error("Something went wrong please try again");
				error.status = 400;
				throw error;
			}
			return res.json({
				info: {
					status: 200,
					message: "Category Added Successfully",
				},
			});
		})
		.catch((error) => {
			next(error);
		});
};

exports.fetchAllCategories = (req, res, next) => {
	Category.find()
		.populate({ path: "createdBy", select: "name -_id" })
		.then((results) => {
			if (!results) {
				const error = new Error("No Categories found");
				error.status = 400;
				throw error;
			}
			res.json({
				info: {
					status: 200,
				},
				categories: results,
			});
		})
		.catch((error) => {
			next(error);
		});
};
exports.deleteCategory = (req, res, next) => {
	const catId = req.params.id;
	Category.findByIdAndRemove({ _id: catId })
		.then((category) => {
			if (!category) {
				const error = new Error("No Such Category found");
				error.status = 400;
				throw error;
			}
			return Product.deleteMany({ category: catId });
		})
		.then((deleted) => {
			if (!deleted) {
				const error = new Error(
					"Category is deleted but products couldn't be deleted please remove them manually"
				);
				error.status = 400;
				throw error;
			}
			return res.json({
				info: {
					status: 200,
					message: "Category and its products have been deleted successfully",
				},
			});
		})
		.catch((error) => {
			next(error);
		});
};
exports.ActivateOrDeactivateCategory = (req, res, next) => {
	const catId = req.params.id;
	const isActive = req.params.isActive;
	Category.findOne({ _id: catId })
		.then((category) => {
			if (!category) {
				const error = new Error("No Such Category found");
				error.status = 400;
				throw error;
			}
			category.isActive = isActive;
			return category.save();
		})
		.then((result) => {
			if (!result) {
				const error = new Error("Something Went wrong try agian");
				error.status = 400;
				throw error;
			}
			res.json({
				info: {
					status: 200,
					message:
						"Category " +
						(isActive == "true" ? "Activated" : "Deactivated") +
						" Successfully",
				},
			});
		})
		.catch((error) => {
			next(error);
		});
};
exports.updateCategory = (req, res, next) => {
	const catId = req.body.id;
	const title = req.body.title;
	let image;
	if (req.file) {
		image = req.file.path;
	}
	if (!title) {
		const error = new Error("Enter Title");
		error.status = 400;
		next(error);
	}
	if (!image) {
		const error = new Error("Image Not Found");
		error.status = 400;
		next(error);
	}
	Category.findOne({ _id: { $ne: catId }, title: title })
		.then((alreadyExists) => {
			if (alreadyExists) {
				const error = new Error("Category Already Exists");
				error.status = 400;
				throw error;
			}
			return Category.findOne({ _id: catId });
		})
		.then((category) => {
			if (!category) {
				const error = new Error("No Such Category found");
				error.status = 400;
				throw error;
			}
			if (category.image !== image) {
				clearImg(category.image);
			}
			category.title = title;
			category.image = image;
			return category.save();
		})
		.then((result) => {
			if (!result) {
				const error = new Error("Something Went wrong try agian");
				error.status = 400;
				throw error;
			}
			res.json({
				info: {
					status: 200,
					message: "Category updated Successfully",
				},
			});
		})
		.catch((error) => {
			next(error);
		});
};
exports.addNewProduct = (req, res, next) => {
	const title = req.body.title;
	const description = req.body.description;
	const stock = req.body.stock;
	const isActive = req.body.isActive;
	const category = req.body.category;
	const createdBy = req.userId;
	const price = req.body.price;
	let image = "";
	if (req.file) {
		image = req.file.path;
	}
	Product.findOne({ category: category, title: title })
		.then((found) => {
			if (found) {
				const error = new Error(
					"A product with same title already exists in this category"
				);
				error.status = 400;
				throw error;
			}
			const product = new Product({
				title: title,
				description: description,
				stock: stock,
				isActive: isActive,
				category: category,
				createdBy: createdBy,
				price: price,
				image: image,
			});
			return product.save();
		})
		.then((product) => {
			return product
				.populate({ path: "createdBy", select: "name -_id" })
				.execPopulate();
		})
		.then((product) => {
			return product.populate("category").execPopulate();
		})
		.then((product) => {
			if (!product) {
				const error = new Error("Something went wrong");
				error.status = 400;
				throw error;
			}
			return res.json({
				info: {
					status: 200,
					message: "Product successfully added",
				},
				product: product,
			});
		})
		.catch((error) => {
			next(error);
		});
};
exports.getProducts = (req, res, next) => {
	const id = req.params.id;
	Product.find({ category: id })
		.populate("category")
		.populate("createdBy", "name -_id")
		.then((products) => {
			if (!products.length) {
				const error = new Error("No products found");
				error.status = 400;
				throw error;
			}
			return res.json({
				info: {
					status: 200,
				},
				products: products,
			});
		})
		.catch((error) => {
			next(error);
		});
};
exports.updateProduct = (req, res, next) => {
	const productId = req.params.id;
	const title = req.body.title;
	const description = req.body.description;
	const stock = req.body.stock;
	const isActive = req.body.isActive;
	const category = req.body.category;
	const price = req.body.price;
	let image;
	if (req.file) {
		image = req.file.path;
	}
	Product.findOne({ _id: productId })
		.then((product) => {
			if (!product) {
				const error = new Error("Product Not Found");
				error.status = 400;
				throw error;
			}
			clearImg(product.image);
			product.title = title;
			product.description = description;
			product.stock = stock;
			product.isActive = isActive;
			product.category = category;
			product.price = price;
			product.image = image;

			return product.save();
		})
		.then((product) => {
			if (!product) {
				const error = new Error("Something went wrong please try again");
				error.status = 400;
				throw error;
			}
			return res.json({
				info: {
					status: 200,
					message: "Product updated successfully",
				},
				product: product,
			});
		})
		.catch((error) => {
			next(error);
		});
};
exports.deleteProduct = (req, res, next) => {
	const id = req.params.id;
	Product.findByIdAndRemove({ _id: id })
		.then((product) => {
			if (!product) {
				const error = new Error("Product Not Found");
				error.status = 400;
				throw error;
			}
			clearImg(product.image);
			return res.json({
				info: {
					status: 200,
					message: "Product Successfully deleted",
				},
			});
		})
		.catch((error) => {
			next(error);
		});
};
exports.getOpenOrders = (req, res, next) => {
	Order.find({ status: "PROCESSING", markRead: true })
		.then((results) => {
			if (!results.length) {
				const error = new Error("No open orders found");
				error.status = 400;
				throw error;
			}
			res.json({
				info: {
					status: 200,
					message: "Successfull",
				},
				orders: results,
			});
		})
		.catch((error) => {
			next(error);
		});
};
exports.getClosedOrders = (req, res, next) => {
	Order.find({ status: { $ne: "PROCESSING" } })
		.then((results) => {
			if (!results.length) {
				const error = new Error("No closed orders found");
				error.status = 400;
				throw error;
			}
			res.json({
				info: {
					status: 200,
					message: "Successfull",
				},
				orders: results,
			});
		})
		.catch((error) => {
			next(error);
		});
};
exports.getNewOrders = (req, res, next) => {
	Order.find({ status: "PROCESSING", markRead: false })
		.then((results) => {
			if (!results.length) {
				/* const error = new Error("No new orders found");
				error.status = 400;
				throw error; */
				return res.json({
					info: {
						status: 400,
						message: "No new orders found",
					},
					orders: results,
				});
			}
			res.json({
				info: {
					status: 200,
					message: "Successfull",
				},
				orders: results,
			});
		})
		.catch((error) => {
			next(error);
		});
};
exports.updateOrderStatus = (req, res, next) => {
	const id = req.body.id;
	const status = req.body.status;
	Order.findOne({ _id: id })
		.then((order) => {
			if (!order) {
				const error = new Error("No such Product Found");
				throw error;
			}
			order.status = status.trim();
			return order.save();
		})
		.then((order) => {
			if (!order) {
				const error = new Error(
					"Error occured while saving the product status"
				);
				throw error;
			}
			res.json({
				info: {
					status: 200,
					message: `Order ${order.status} successfully`,
				},
			});
		})
		.catch((error) => {
			next(error);
		});
};
exports.getNewOrdersCount = (req, res, next) => {
	Order.countDocuments({ status: "PROCESSING", markRead: false })
		.then((count) => {
			res.json({
				info: {
					status: 200,
				},
				count: count,
			});
		})
		.catch((error) => {
			next(error);
		});
};
exports.markAsReadOrders = (req, res, next) => {
	const ids = req.body.ids;
	Order.updateMany({ _id: { $in: ids } }, { markRead: true }, { multi: true })
		.then((updated) => {
			io.getIO().emit("markedAsRead", { action: "updated order" });
			res.json({
				info: {
					status: 200,
					message: "Orders successfully marked as read",
				},
			});
		})
		.catch((error) => {
			next(error);
		});
};
const clearImg = (filePath) => {
	if (!filePath) {
		return;
	}
	filePath = path.join(__dirname, "..", filePath);
	fs.unlink(filePath, (error) => {});
};
