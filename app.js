const express = require("express");
const mongoose = require("mongoose");
const multer = require("multer");
const path = require("path");
var cors = require("cors");
const port = process.env.PORT || 8080;
const app = express();
const { createServer } = require("http");
const httpServer = createServer(app);
const adminRoutes = require("./Routes/admin");
const userRoutes = require("./Routes/user");
/* Parser Config */
const fileStorage = multer.diskStorage({
	destination: (req, file, cb) => {
		if (
			req.url === "/admin/updateCategory" ||
			req.url === "/admin/addNewCategory"
		) {
			cb(null, "images/categories");
		} else {
			cb(null, "images/products");
		}
	},
	filename: (req, file, cb) => {
		cb(null, file.originalname);
	},
});
const imageFilter = (req, file, cb) => {
	if (
		file.mimetype === "image/png" ||
		file.mimetype === "image/jpeg" ||
		file.mimetype === "image/jpg"
	) {
		cb(null, true);
	} else {
		cb(null, false);
	}
};
/* CORS */
app.use(
	cors({
		origin: ["https://supplementpro.pk", "https://admin.supplementpro.pk"],
		methods: ["GET", "POST", "DELETE", "UPDATE", "PUT", "PATCH"],
		credentials: true,
	})
);
/*app.use((req, res, next) => {
	res.setHeader("Access-Control-Allow-Origin", "https://supplementpro.pk");
	res.setHeader(
		"Access-Control-Allow-Methods",
		"OPTIONS , PUT ,POST,PATCH,DELETE,GET"
	);
	res.setHeader("Access-Control-Allow-Headers", "Content-Type , Authorization");
	next();
});*/
/* Parsers */
app.use(express.json());
app.use(
	multer({ storage: fileStorage, fileFilter: imageFilter }).single("image")
);
/* Routes */
app.use("/admin/images", express.static(process.env.HOME + "/images"));
app.use("/user/images", express.static(process.env.HOME + "/images"));
app.use("/admin", adminRoutes);
app.use("/user", userRoutes);

/* Errors */
app.use((error, req, res, next) => {
	error.status = error.status ? error.status : 500;
	res.json({
		info: {
			status: error.status,
			message: error.message,
		},
		error: error,
	});
});
mongoose
	.connect(
		"mongodb+srv://Azam:azmnwz0074993434@cluster0.ufqei.mongodb.net/Nutrion?retryWrites=true&w=majority",
		{
			useNewUrlParser: true,
			useUnifiedTopology: true,
			useFindAndModify: false,
			useCreateIndex: true,
		}
	)
	.then((client) => {
		/* app.listen(8080); */
		/* const server = app.listen(8080); */
		/* console.log(client) */
		const io = require("./socket").init(httpServer);
		httpServer.listen(port);
		io.on("connection", (socket) => {
			/* console.log("client connected"); */
		});
	})
	.catch((err) => {
		console.log(err);
	});
