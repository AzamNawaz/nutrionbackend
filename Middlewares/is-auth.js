const jwt = require("jsonwebtoken");

exports.isAuth = (req, res, next) => {
	const headers = req.get("Authorization");
	if (!headers) {
		const error = new Error("No Authentication key found on request");
		error.status = 400;
		throw error;
	}
	const token = headers.split(" ")[1];
	if (token == 'null' || token == 'undefined') {
		const error = new Error("No Authentication key found on request");
		error.status = 400;
		throw error;
	}
	let tokenDecoded;
	try {
		tokenDecoded = jwt.verify(token, "mysecretkey");
	} catch {
		const error = new Error("Your login session expired");
		error.status = 400;
		throw error;
	}
	if (!tokenDecoded) {
		const error = new Error("Your not Authorized");
		error.status = 400;
		throw error;
	}
	req.userName = tokenDecoded.name;
	req.userId = tokenDecoded.userId;
	next();
};

exports.verifyToken = (req, res, next) => {
	const headers = req.get("Authorization");
	if (!headers) {
		const error = new Error("No Headers Attached");
		error.status = 400;
		throw error;
	}
	const token = headers.split(" ")[1];
	let tokenDecoded;
	try {
		tokenDecoded = jwt.verify(token, "mysecretkey");
	} catch {
		const error = new Error("Session expired");
		error.status = 400;
		throw error;
	}
	if (!tokenDecoded) {
		const error = new Error("Your not Authorized");
		error.status = 400;
		throw error;
	}
	res.json({
		info: {
			status: 200,
		},
	});
};
