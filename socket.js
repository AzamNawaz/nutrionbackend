let io;
const { Server } = require("socket.io");

module.exports = {
	init: (server) => {
		io = new Server(server, {
			cors: {
				origin: "*",
				methods: ["GET", "POST"],
			},
		});
		/* 	console.log(io) */
		return io;
	},
	getIO: () => {
		if (!io) {
			throw new Error("No Connection Found");
		}
		return io;
	},
};
