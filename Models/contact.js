const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const contactSchema = new Schema({
	firstName: { type: String, required: true },
	lastName: { type: String, required: true },
	email: { type: String, required: true },
	mobile: { type: String, required: true },
	subject: { type: String, required: true },
	description: { type: String, required: true },
});

module.exports = mongoose.model("contact", contactSchema);
