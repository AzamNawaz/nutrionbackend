const mongoDb = require("mongoose");
const Schema = mongoDb.Schema;

const userSchema = new Schema({
	email: { type: String, required: true },
	name: { type: String, required: true },
	password: { type: String, required: true },
});

module.exports = mongoDb.model('admin',userSchema)