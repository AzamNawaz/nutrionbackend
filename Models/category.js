const mongoDb = require("mongoose");
const Schema = mongoDb.Schema;

const categorySchema = new Schema(
	{
		title: { type: String, required: true },
		id: { type: Number, autoIncrement: true },
		image: { type: String, required: true },
		isActive: { type: Boolean, required: true },
		createdBy: { type: Schema.Types.ObjectId, ref: "admin" },
	},
	{
		timestamps: true,
	}
);

module.exports = mongoDb.model("category", categorySchema);
