const mongoDb = require("mongoose");
const Schema = mongoDb.Schema;

const productSchema = new Schema(
	{
		title: { type: String, required: true },
		description: { type: String, required: true },
		stock: { type: Boolean, required: true },
		id: { type: Number, autoIncrement: true },
		isActive: { type: Boolean, required: true },
		price: { type: Number, required: true },
		image: { type: String },
		units: { type: String },
		category: { type: Schema.Types.ObjectId, ref: "category", required: true },
		createdBy: { type: Schema.Types.ObjectId, ref: "admin", required: true },
	},
	{
		timestamps: true,
	}
);

module.exports = mongoDb.model("product", productSchema);
