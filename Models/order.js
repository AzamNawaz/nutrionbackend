const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const orderSchema = new Schema(
	{
		shippingDetails: { type: Schema.Types.Mixed, required: true },
		userDetails: { type: Schema.Types.Mixed, required: true },
		markRead: { type: Boolean, required: true, default: false },
		status: {
			type: String,
			required: true,
			default: "PROCESSING",
			uppercase: true,
		},
		deliveryCharges: { type: Number, required: true, default: 0 },
		products: { type: Array, required: true },
		total: { type: Number, required: true },
	},
	{
		timestamps: true,
	}
);
module.exports = mongoose.model("order", orderSchema);

/* orderId: {
			type: Number,
			createIndexes: true,
			unique: true,
			required: true,
			autoPopulate: true,
		}, */
